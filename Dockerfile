FROM ruby:3.1

WORKDIR /ios

COPY . .

RUN gem install bundler:2.3.11
RUN gem install fastlane:2.211.0
